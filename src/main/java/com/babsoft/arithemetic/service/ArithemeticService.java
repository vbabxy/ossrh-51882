package com.babsoft.arithemetic.service;

public interface ArithemeticService {

    double add(double value1, double value2);
}
